import conexion from "./conexion.js";
import { rejects } from "assert";
var alumnosDb = {}
alumnosDb.insertar = function insertar(alumno){
    return new Promise((resolve,rejects)=>{
        // consulta
        let sqlConsulta = "Insert into sistemas.alumnos set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){

        if(err){

            console.log("Surgio un error ",err.message);
            rejects(err);

        }else {

            const alumno = {
            id:res.id,

            }
            resolve(alumno);
        }

        });
    });
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select * from sistemas.alumnos";
        conexion.query(sqlConsulta, null, function(err, res) {
            if(err) {

            console.log("Surgio un error");
            reject(err);

            }else {

                resolve(res);

            }
        });
    });
}

export default alumnosDb;