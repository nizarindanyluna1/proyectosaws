/*
const express = require('express');
const http = require('http');       //Importando framework
const puerto = 3000;

const app = express();

app.get('/',(req,res)=>{

    res.send("Iniciando la pagina");

});

app.listen(puerto,()=>{

    console.log("Iniciando el servidor" + puerto);

})
*/

const express = require('express');
const http = require('http');
const puerto = 3000;
const json = require('body-parser');
const app = express();
//Asignaciones
app.set("view engine","ejs");
app.use(json.urlencoded({extends:true}));

//Asignar al objeto informacion
app.set(express.static(__dirname + '/public'));

app.get('/',(req,res)=>{
    res.render('index',{titulo:"Mi primera pagina ejs",nombre:"Nizarindany Luna"});
});

app.get('/tabla',(req,res)=>{

    const params = {
        numero : req.query.numero
    }
    res.render('tabla',params);
})

app.post('/tabla',(req,res)=>{
    
    const params = {
        numero : req.body.numero
    }
    res.render('tabla',params);
})


app.listen(puerto,()=>{
    console.log("Iniciando el servidor por el puerto " + puerto);
});